import React, { useState } from "react";
import { data_shoes } from "../asset/dataShoes/dataShoes";
export default function GioHangHook() {
  let [cart, setCard] = useState([]);
  let handleAddCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((e) => {
      return e.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, quality: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quality++;
    }
    setCard((cart = cloneCart));
  };
  let handleChangeQuality = (id, num) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((e) => {
      return e.id == id;
    });
    cloneCart[index].quality = cloneCart[index].quality + num;
    if (cloneCart[index].quality == 0) {
      cloneCart.splice(index, 1);
    }
    setCard((cart = cloneCart));
  };
  let renderShoes = () => {
    return data_shoes.map((item, index) => {
      return (
        <div key={index} className="card col-3">
          <img className="card-img-top" src={item.image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{item.name}</h5>
            <p className="card-text">{item.shortDescription}</p>
            <button
              onClick={() => {
                handleAddCart(item);
              }}
              className="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        </div>
      );
    });
  };
  let handleRenderTbody = () => {
    return cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: 40 }} alt="" />
          </td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                handleChangeQuality(item.id, -1);
              }}
              type="button"
              class="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{item.quality}</span>
            <button
              onClick={() => {
                handleChangeQuality(item.id, 1);
              }}
              type="button"
              class="btn btn-success"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <div className="container my-5">
      <div className="table">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Image</th>
              <th>Price</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>{handleRenderTbody()}</tbody>
        </table>
      </div>
      <div className=" row">{renderShoes()}</div>
    </div>
  );
}
